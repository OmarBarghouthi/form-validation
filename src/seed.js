export const seedData = [
    {
      name: "First Name",
      id: 'firstName',
      validationMessage: 'Monday',
      placeHolder: 'Omar',
      class: 'firstName',
      type: 'text'
    },
    {
        name: 'Last Name',
        id: 'lastName',
        validationMessage: 'Monday',
        placeHolder: 'Barghouthi',
        class: 'lastName',
        type: 'text'
    },
    {
        name: 'Username',
        id: 'userName',
        validationMessage: 'Monday',
        placeHolder: 'obarghouthi',
        class: 'userName',
        type: 'text'
    },
    {
        name: 'Email',
        id: 'email',
        validationMessage: 'Monday',
        placeHolder: 'example@gmail.com',
        class: 'email',
        type: 'email'
    },
    {
        name: 'Address One',
        id: 'addressOne',
        validationMessage: 'Monday',
        placeHolder: 'address',
        class: 'addressOne',
        type: 'text'
    },
    {
        name: 'Address Two',
        id: 'addressTWo',
        validationMessage: 'Monday',
        placeHolder: 'address two',
        class: 'addressTwo',
        type: 'text'
    }
  ]
  